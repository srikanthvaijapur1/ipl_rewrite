//internal functions
const csvtojson = require("csvtojson");
const fs = require("fs");

//data files
const matchesFilePath = "src/data/matches.csv";
const deliveriesFilePath = "src/data/deliveries.csv";

//IPL modules

const matchesPlayedPerYear = require("./iplFunctions/matchesPlayedPerYear.js");
const matchesWonPerYear = require("./iplFunctions/matchesWonPerYear");
const extraRunsByTeam = require("./iplFunctions/extraRunsByTeam");
const topEconomicBowlers = require("./iplFunctions/topEconomyBowlers");

//output files

const matchesPerYearJson = "src/public/output/matchesPerYear.json";
const matchesWonPerTeamPerYearJson = "src/public/output/matchesWonPerTeamPerYear.Json";
const extraRunsConcededPerTeamJson = "src/public/output/extraRunsConcededPerTeam.Json";
const topEconomicBowlersJson = "src/public/output/topEconomicBowlers.json";


function main() {
    csvtojson()
        .fromFile(matchesFilePath)
        .then((matches) => {
            csvtojson()
                .fromFile(deliveriesFilePath)
                .then((deliveries) => {
                   
                    let matchesPlayedPerYearResult = matchesPlayedPerYear(matches);
                    let matchesWonPerYearResult = matchesWonPerYear(matches);
                    let extraRunsByTeamResult = extraRunsByTeam(matches, deliveries, '2016');
                    let topEconomicBowlersResult = topEconomicBowlers(matches, deliveries, '2015');
                    


                    saveOutputResults(matchesPlayedPerYearResult, matchesPerYearJson);
                    saveOutputResults(matchesWonPerYearResult, matchesWonPerTeamPerYearJson);
                    saveOutputResults(extraRunsByTeamResult, extraRunsConcededPerTeamJson);
                    saveOutputResults(topEconomicBowlersResult, topEconomicBowlersJson);
                })
        })
}


//function to save output to newfile
const saveOutputResults = (results, path) => {
    const jsonData = { results }
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(path, jsonString, "utf8", (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`succesfully saved "${path}" file`);
        }
    });
};

main();