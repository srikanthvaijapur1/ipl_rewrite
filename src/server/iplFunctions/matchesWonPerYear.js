// Number of matches won per team per year in IPL.

const matchesWonPerYear = (matches) => {

    let result = matches.reduce((acc, match) => {
        if (acc[match.season] === undefined) {
            acc[match.season] = {};
        }
        if (acc[match.season][match.winner] === undefined) {
            acc[match.season][match.winner] = 1;
        } else {
            acc[match.season][match.winner] += 1;
        }

        return acc;

    }, {});

    return result;
}


module.exports = matchesWonPerYear;