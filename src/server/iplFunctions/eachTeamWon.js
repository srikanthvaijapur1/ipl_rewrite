// Find the number of times each team won the toss and also won the match

const eachTeamWonTossAndMatch = (matches) => {
    let result = matches.reduce((acc, match) => {
        if (match.winner === match.toss_winner) {
            if (acc[match.winner]) {
                acc[match.winner] += 1;
            } else {
                acc[match.winner] = 1;
            }
        }
        return acc;
    }, {})
    return result;
}


module.exports = eachTeamWonTossAndMatch;