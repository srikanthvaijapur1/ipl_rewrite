const extraRunsByTeam = (matches, deliveries, year) => {
    const ids = matches.filter(match => match.season === year).map(obj => (obj.id))

    const deliveriesData = deliveries.filter(data => ids.includes(data.match_id))

    let result = deliveriesData.reduce((acc, deleivery) => {
        if (acc[deleivery.bowling_team] === undefined) {
            acc[deleivery.bowling_team] = parseInt(deleivery.extra_runs)

        } else {
            acc[deleivery.bowling_team] += parseInt(deleivery.extra_runs);
        }
        return acc
    }, {})

    return result;
}


module.exports = extraRunsByTeam;