const matchesPlayedPerYear = (matches) => {

    let result = matches.reduce((acc, match) => {
        if (acc[match.season] !== undefined) {
            acc[match.season] += 1;
        } else {
            acc[match.season] = 1;
        }
        return acc;
    }, {})
    return result;
}

module.exports = matchesPlayedPerYear;
