// Top 10 economical bowlers in the year 2015

const topEconomicBowlers = (matches, deliveries, year) => {

    let ids = matches.filter(match => match.season === year).map(match => match.id);
    let deliveryData = deliveries.filter(delivery => ids.includes(delivery.match_id));

    let economyBowlers = deliveryData.reduce((economy, delivery) => {

        let bowler = delivery.bowler;

        if (economy[bowler]) {
            economy[bowler].balls += 1;
            economy[bowler].runs += parseInt(delivery.total_runs);
            economy[bowler].economyRate = (economy[bowler].runs / ((economy[bowler].balls / 6)).toFixed(3));
        } else {
            economy[bowler] = {};
            economy[bowler].balls = 1;
            economy[bowler].runs = parseInt(delivery.total_runs);
            economy[bowler].economyRate = (economy[bowler].runs / ((economy[bowler].balls / 6)).toFixed(3));
        }
        return economy;
        
    }, {})

    let topTenEconomyBowlers = Object.entries(economyBowlers).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(1, 11);
    let result = Object.fromEntries(topTenEconomyBowlers);

    return result;
}


module.exports = topEconomicBowlers;